import numpy
import numpy as np
import tensorflow as tf
from string import punctuation
from keras import regularizers
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import *
from nltk import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.layers import Dense
from sklearn.neighbors import KNeighborsClassifier


def scan_scv(filename):
    res = list()
    with open(filename, encoding="utf-8") as fin:
        string = ""
        next(fin)
        for line in fin:
            line = line.rstrip("\n")
            if re.fullmatch(r"\",\d.\d", line) is None:
                string += line
            else:
                res.append([string, int(float(re.search(r"\d.\d", line)[0]))])
                string = ""
    return np.array(res)


def clean_messages(messages):
    re_puncs = re.compile('[%s]' % re.escape(punctuation))
    stop_words = set(stopwords.words('russian'))
    lem = WordNetLemmatizer()
    p_stem = PorterStemmer()
    res = list()
    for message in messages:
        tokens = word_tokenize(message)
        tokens = [w.lower() for w in tokens]
        tokens = [re_puncs.sub('', w) for w in tokens]
        tokens = [i for i in tokens if i.isalpha()]
        tokens = [w for w in tokens if w not in stop_words]
        tokens = [lem.lemmatize(w) for w in tokens]
        tokens = [p_stem.stem(w) for w in tokens]
        res.append(' '.join(tokens))
    return res


def confusion_matrix(real, predicted, target):
    res = np.zeros((2, 2))
    for i in range(len(real)):
        if real[i] == target:
            col = 1
        else:
            col = 0
        if predicted[i] == target:
            line = 1
        else:
            line = 0
        res[line][col] += 1
    return res


def print_stats(real, predicted):
    print("Confusion matrix:")
    for target in set(tuple(i) for i in real):
        mat = confusion_matrix([tuple(i) for i in real], [tuple(i) for i in predicted], target)
        print(mat)
        recall = mat[0][0] / (mat[0][0] + mat[1][0])
        print("Recall:", recall)
        precision = mat[0][0] / (mat[0][0] + mat[0][1])
        print("Precision:", precision)
        print("Accuracy:", (mat[0][0] + mat[1][1]) / (len(real)))
        print("F-measure:", 2 * recall * precision / (recall + precision))


if __name__ == "__main__":
    train_ratio = 0.9
    data = scan_scv('labeled.csv')
    np.random.shuffle(data)
    train = data[:int(len(data) * train_ratio)]
    test = data[int(len(data) * train_ratio):]

    train = np.transpose(train)
    [comments], [toxicity] = np.split(train, 2, 0)
    comments = clean_messages(comments)
    toxicity_train = tf.keras.utils.to_categorical(toxicity, len(set(toxicity)))
    vectorizer = TfidfVectorizer()
    train_vec = vectorizer.fit_transform(comments).toarray()

    neural_net = tf.keras.Sequential([
        Dense(1024, input_dim=len(train_vec[0]), activation='relu', kernel_regularizer=regularizers.l2(0.001), ),
        Dense(512, activation='relu', kernel_regularizer=regularizers.l2(0.001), ),
        Dense(256, activation='relu', kernel_regularizer=regularizers.l2(0.001), ),
        Dense(128, activation='relu', kernel_regularizer=regularizers.l2(0.001), ),
        Dense(64, activation='relu', kernel_regularizer=regularizers.l2(0.001), ),
        Dense(len(set(toxicity)), activation='softmax')
    ])
    neural_net.compile(
        loss='categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy']
    )

    neural_net.fit(train_vec, toxicity_train, epochs=6, validation_split=0.1)

    k_neighbours = KNeighborsClassifier()
    k_neighbours.fit(train_vec, toxicity_train)

    test = np.transpose(test)
    [comments], [toxicity] = np.split(test, 2, 0)
    comments = clean_messages(comments)
    toxicity_test = tf.keras.utils.to_categorical(toxicity, len(set(toxicity)))
    test_vec = vectorizer.transform(comments).toarray()

    score = neural_net.evaluate(test_vec, toxicity_test)
    toxicity_pred = k_neighbours.predict(test_vec)

    print("Neural net:\nLoss:", score[0], "\tAccuracy:", score[1])
    print("\nKNeighbours result:\n")
    print_stats(toxicity_pred, toxicity_test)

